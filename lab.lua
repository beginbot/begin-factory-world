local stolen        = require("stolen")

local function coal_time(player, args)
    local entity = "coal"
    local amount = args[1]
    if amount == nil then
      amount = 5
    end
    local furnace_slot = defines.inventory.fuel

    for x=-20,20 do
      for y=-20,20 do
          stolen.put(player, {player.position.x-x, player.position.y-y}, entity, amount, furnace_slot)
      end
    end
end


local function iron_time(player, args)
    local entity = "iron-ore"
    local amount = args[1]
    if amount == nil then
      amount = 5
    end
    local furnace_slot = defines.inventory.furnace_source

    for x=-20,20 do
      for y=-20,20 do
          stolen.put(player, {player.position.x-x, player.position.y-y}, entity, amount, furnace_slot)
      end
    end
end


return {
  coal_time = coal_time,
  iron_time = iron_time,
}
