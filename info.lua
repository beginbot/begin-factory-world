local notifications = require("notifications")

local function tech_list(player, _)
  local list = {}
    for _, tech in pairs(player.force.technologies) do
    if tech.researched then
      list[#list+1] = tech.name
    end
  end
  game.write_file("tech.lua", serpent.block(list) .. "\n", true)
  local tech = table.concat(list, ", ")
  local msg = string.format("Available Tech: %s", tech)
  notifications.notify(msg)
  return list
end

local function knows_tech(list, tech)
  for _, kt in pairs(list) do
    if kt == tech  then
      return true
    end
  end
  return false
end

local function unknown_tech_list(player, _)
  local known_tech = tech_list(player)

  local list = {}
  for _, tech in pairs(player.force.technologies) do
    local has_reqs = true

    -- Check we have all pre reqs
    for _, t in pairs(tech.prerequisites) do
      local fills_req = knows_tech(known_tech, t)
      if not fills_req then
        has_reqs = false
      end
    end

    -- not and in lua
    if not tech.researched and has_reqs then
      print(tech.name)
      list[#list+1] = tech.name
    end

    if #list > 10 then
      local tech_str = table.concat(list, ", ")
      local msg = string.format("Tech to research Tech: %s", tech_str)
      notifications.notify(msg)
      list = {}
    end
  end

  if #list > 0  then
    local tech_str = table.concat(list, ", ")
    local msg = string.format("Tech to research Tech: %s", tech_str)
    notifications.notify(msg)
  end
  -- game.write_file("tech.lua", serpent.block(list) .. "\n", true)
end

local function recipe_list(player, _)
  local list = {}
  for _, recipe in pairs(player.force.recipes) do
    if recipe.enabled then
      list[#list+1] = recipe.name
    end
  end

  game.write_file("recipes.lua", serpent.block(list) .. "\n", true)
  local recipes = table.concat(list, ", ")
  local msg = string.format("Available Recipes: %s", recipes)
  notifications.notify(msg)
end

return {
  recipe_list       = recipe_list,
  tech_list         = tech_list,
  unknown_tech_list = unknown_tech_list,
}
