--control.lua

require ('util')
require ('story')

local console_chat  = require("console_chat")
local character     = require("character")
local notifications = require("notifications")
local goals         = require("goals")
local player_finder = require("player_finder")
local missing       = require("missing")

require("direction")


-- Is this ok??
-- I think this is on game init
script.on_init(function() -- STARTING THE MOD
  game.print("Starting Script!")
  global["firemode"] = nil
  global["oilmode"]  = nil
  global["nuclear"]  = nil
  global["killmode"] = nil
end)


local function set_default_cache()
  local c = global["cache"]
  if c == nil then
    c = {
      ["build"] = {"stone-furnace"},
    }
    global["cache"] = c
  else
    if next(c) == nil then
      c = {
        ["build"] = {"stone-furnace"},
      }
      global["cache"] = c
    end
  end
end

-- local cache         = require("cache")
set_default_cache()

script.on_event(defines.events.on_console_chat, -- EVERY TIME WE GET CONSOLE CHAT
  function(event)

    local success, message = pcall(console_chat.handle, event)
    if message ~= nil then
      print("Message: " .. message)
      notifications.notify(message)
    end

    if success then
      print("Success")
    else
      game.print(message)
      print("Failure")
    end
end)


script.on_event(defines.events.on_player_changed_position, -- EVERY TIME THE PLAYER MOVES
  function(event)
    local player = player_finder.find()

    if player ~= nil then
      pcall(character.maxhealth, player)
      if global["firemode"] == true then
        player.surface.create_entity{name="fire-flame", position=player.position, force="neutral"}
      end

      if global["nuclear"] == true then
        player.surface.create_entity(
          {
            name='uranium-ore',
            amount=100000,
            position={player.position.x, player.position.y}
          }
        )
      end

      if global["oilmode"] == true then
        player.surface.create_entity(
          {
            name='crude-oil',
            amount=100001,
            position={player.position.x, player.position.y}
          }
        )
      end
    end

    if global["killmode"] == true then
      game.forces["enemy"].kill_all_units()
    end

  end)


-- script.on_event(defines.events.on_tick, function(_) -- EVERY TICK
-- end)

script.on_event(defines.events.on_player_cancelled_crafting, function(event)
    local player = player_finder.find()

    if player ~= nil then
      local msg = string.format("Cancelled %s", event.recipe.name)
      notifications.flying_text(player, msg)
   end
end)


script.on_event(defines.events.on_pre_player_crafted_item, function(event)
    local player = player_finder.find()

    if player ~= nil then
      local msg = string.format("Pre Craft %s", event.recipe.name)
      notifications.flying_text(player, msg)
   end
end)


script.on_event(defines.events.on_player_crafted_item, function(event)
    local player = player_finder.find()

    if player ~= nil then
      local msg = string.format("Crafted %s", event.recipe.name)
      notifications.flying_text(player, msg)
      -- notifications.notify(msg)
   end
end)

script.on_nth_tick(900, -- every 15 seconds
  function(_)
    local player = player_finder.find()
    if player ~= nil then
      local msg = string.format("X %d Y %d", player.position.x, player.position.y)
      notifications.flying_text(player, msg)
    end
  end)

script.on_event(defines.events.on_console_command, -- EVERY TIME WE CONSOLE COMMANDS
  function(_)
end)

local story_table =
{
  {
    {
      name = 'setup',
      init = function()
      end
    },
    {
      name = 'sunrise',
      init = function()
      end,
      update = function()
      end
    },
    {
      name = 'think-2',
      condition = story_elapsed_check(2),
      action = function()
        print("\t\tTHINKING")
        -- TODO: Do we need to set the goals over and over
        goals.set_goal("Collect the Materials to Beat the Level")
        set_info({custom_function = goals.update_materials_gui})
      end

    },
    {
      name = 'movement',
      init = function()
        -- goals.set_goal("Collect 5 Coal\nCollect 5 Stone\nCollect 5 Iron-Ore")
        goals.set_goal("Collect the Materials to Beat the Level")
      end,
      condition = function()
        set_info({custom_function = goals.update_materials_gui})
        return goals.has_completed_goals()
      end,
      action = function()
        print("Running movement branch")
      end
    },
  }
}

story_init_helpers(story_table)

-- Anytime one of these methods
local story_events =
{
  defines.events.on_player_cursor_stack_changed,
  defines.events.on_player_main_inventory_changed,
  defines.events.on_gui_opened,
  defines.events.on_gui_closed,
  defines.events.on_picked_up_item,
  defines.events.on_player_mined_entity,
  defines.events.on_player_mined_item,
  defines.events.on_built_entity,
  defines.events.on_player_rotated_entity,
  defines.events.on_selected_entity_changed,
  defines.events.on_pre_player_crafted_item,

  -- defines.events.on_tick
}


-- This is to update the "character"
-- from a chat command
-- stop or continue their state
local function update_chat_set_state(_)
  -- print("Set Chat State" .. tick)
  local player = player_finder.find()

  if player == nil then
    print("PLAYER IS NIL WHILE SETTING CHAT STATE???!?!")
    return
  end

  if global["minemode"] == true then
    print("Mine Mode!!!!")
    player.update_selected_entity(player.position)
    player.mining_state = {mining = true, position = player.position}
  end

  if global["player.walking"] ~= nil then
    print("WALKING MODE!!!!")
    Pos = global["player.walking"]

    if Pos.x ~= nil and Pos.y ~= nil then
      print("POS IS NOT NIL")
      -- What if we overshoot the walking??
      if (math.abs(player.position.x - Pos.x) < 0.5) and
         (math.abs(player.position.y - Pos.y) < 0.5) then
        global["player.walking"] = nil
        player.walking_state = {walking = false, direction = player.character.direction}
      end
    else
      print("POS IS NOT NIL")
    end
  end
end


script.on_init(function()
  print("ON INIT")

  if global.story == nil then
    print("Initialize Story")
    -- goals.set_goal("Collect 5 Coal\nCollect 5 Stone\nCollect 5 Iron-Ore")
    goals.set_goal("Collect the Materials to Beat the Level")
    global.story = story_init()
    set_info({custom_function = goals.update_materials_gui})
  else
    print("NOT Initialize Story")
  end
end)



script.on_event(defines.events.on_tick, function(event)
  -- print(missing.inspect(event))
  update_chat_set_state(tostring(event.tick))

  if not game.tick_paused then
    if global.story ~= nil then
      story_update(global.story, event, "level-02")
    end
  end
end)

script.on_event(story_events, function(event)
  -- update_chat_set_state()

  if not game.tick_paused then
    if global.story ~= nil then
      story_update(global.story, event, "level-02")
    end
  end


end)
