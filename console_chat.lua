local cheating      = require("cheating")
local direction     = require("direction")
local build         = require("build")
local string_utils  = require("string_utils")
local notifications = require("notifications")
local markers       = require("markers")
local box           = require("box")
local info          = require("info")
local lab           = require("lab")
local player_finder = require("player_finder")

local function toggle_gui(player, _)
    local current = player.game_view_settings.show_controller_gui
    local reverse = current == false
    player.game_view_settings = { show_controller_gui = reverse }
  end

local function toggle_research(player, _)
    local current = player.game_view_settings.show_research_info
    local reverse = current == false
    player.game_view_settings = { show_research_info = reverse }
end

local function toggle_entity(player, _)
    local current = player.game_view_settings.show_entity_info
    local reverse = current == false
    player.game_view_settings = { show_entity_info = reverse }
end

-- show_controller_gui           = false,
local function hide_all_menus(player, _)
    player.game_view_settings =
    {
      show_research_info            = false,
      show_entity_info              = false,
      update_entity_selection       = false,

      show_minimap                  = false,
      show_alert_gui                = false,
      show_rail_block_visualisation = false,
      show_side_menu                = false,
      show_map_view_options         = false,
      show_controller_gui           = false,
      show_quickbar                 = false,
      show_shortcut_bar             = false,
    }
end



local function show_all_menus(player, _)
    player.game_view_settings =
    {
      show_controller_gui           = true,
      show_minimap                  = true,
      show_research_info            = true,
      show_entity_info              = true,
      show_alert_gui                = true,
      update_entity_selection       = true,
      show_rail_block_visualisation = true,
      show_side_menu                = true,
      show_map_view_options         = true,
      show_quickbar                 = true,
      show_shortcut_bar             = true,
    }
end

-- Testing this out
local function create_player(player, _)
  -- We could make this more random
	local entity = player.surface.create_entity{
    name = "character",
    position = {(player.position.x - 2), (player.position.y - 3)},
    force = game.forces.player
  }

  -- This is disassociating with the OG character
  -- Can we control the user are using???
  -- game.player.character=nil
  -- game.player.character=entity
  -- print("Creating New Character " .. entity.name)
end

-- Every single function in this table
-- NEEDS to take in a player and an args table
local chat_commands = {
  ["club"] = create_player,

  -- DRAWING BOXES
  ["wb"]       = box.west_box,
  ["nb"]       = box.north_box,
  ["eb"]       = box.east_box,
  ["sb"]       = box.south_box,

  -- MOVING AROUND
  ["dir"]          = direction.dir,
  ["n"]            = direction.move_north,
  ["north"]        = direction.move_north,
  ["s"]            = direction.move_south,
  ["south"]        = direction.move_south,
  ["e"]            = direction.move_east,
  ["east"]         = direction.move_east,
  ["w"]            = direction.move_west,
  ["west"]         = direction.move_west,
  ["ne"]           = direction.move_northeast,
  ["northeast"]    = direction.move_northeast,
  ["nw"]           = direction.move_northwest,
  ["northwest"]    = direction.move_northwest,
  ["se"]           = direction.move_southeast,
  ["southeast"]    = direction.move_southeast,
  ["sw"]           = direction.move_southwest,
  ["southwest"]    = direction.move_southwest,
  ["stop"]         = direction.stop,
  ["backwards"]    = direction.backwards,
  ["bw"]           = direction.backwards,
  ["~"]            = direction.stop_everything,

  -- BASIC BUILDING FUNCTIONS
  ["b"]        = build.build_entity,
  ["build"]    = build.build_entity,
  ["c"]        = build.craft_entity,
  ["craft"]    = build.craft_entity,
  ["d"]        = build.destroy,
  ["destroy"]  = build.destroy,
  ["mine"]     = build.mine,
  ["m"]        = build.mine,
  ["stopmine"] = build.stopmine,
  ["take"]     = build.take,
  ["rotate"]   = build.rotate,
  ["rot"]      = build.rot,
  ["rrot"]     = build.rrot,
  ["flip"]     = build.flip,
  ["p"]        = build.put_item,
  ["put"]      = build.put_item,
  ["lay"]      = build.lay_items,
  ["l"]        = build.lay_items,
  ["recipe"]      = build.set_recipe,
  ["move"]      = build.move,

  -- Science
  ["research"]         = build.research,

  -- CHEATING
  ["create"]   = cheating.create,

  ["friend"]   = cheating.friend,
  ["killall"]  = cheating.killall,
  ["killmode"] = cheating.killmode,

  ["biters"]   = cheating.create_biters,
  ["firemode"] = cheating.firemode,
  ["oilmode"]  = cheating.oilmode,
  ["nuclear"]  = cheating.nuclear,
  ["nuke"]  = cheating.nuclear_explosion,
  ["squad"]  = cheating.squad_up,
  ["destroysquad"]  = cheating.destroy_squad,

  ["launch"]   = cheating.launch,
  ["reset"]    = cheating.reset,
  ["teleport"] = cheating.teleport,
  ["tele"]     = cheating.teleport,
  ["tel"]      = cheating.tel,
  ["htr"]      = cheating.hit_the_road,
  ["htr2"]     = cheating.hit_the_road2,
  ["reach"]     = cheating.enable_long_reach,

  -- INFO BACK TO CHAT
  ["recipes"]  = info.recipe_list,
  ["tech"]     = info.tech_list,
  ["subjects"] = info.unknown_tech_list,
  ["banner"]   = notifications.banner,
  ["ingred"]   = build.find_ingredients,

  -- MARKERS
  ["markers"] = markers.find_markers,
  ["marker"]  = markers.create_marker,
  ["mark"]    = markers.create_marker,
  ["map"]     = markers.toggle_map,

  -- TOO SPECIFIC
  ["iron"]     =  lab.iron_time,
  ["coal2"]     = lab.coal_time,

  -- MENUS
  ["show_menus"]      = show_all_menus,
  ["hide_menus"]      = hide_all_menus,
  ["toggle_gui"]      = toggle_gui,
  ["toggle_entity"]   = toggle_entity,
  ["toggle_research"] = toggle_research,

}

local do_not_cache = {
  "base",
  "tele",
  "teleport",
}

local function exec(player, cmd, args)
    local chat_func = chat_commands[cmd]
    local cache = global["cache"]

    if next(args) == nil and do_not_cache[cmd] == nil then
      args = cache[cmd]
      if args == nil then
        args = {}
      end
    end

    if chat_func ~= nil then
      if player ~= nil then
        print("Executing Command " .. cmd .. " for " .. player.name)

        if next(args) ~= nil then
          cache[cmd] = args
          global["cache"] = cache
        end

        return chat_func(player, args)
      end
    end
end


local function handle(event)
    local commands = string_utils.mysplit(event.message, "|")
    local player = player_finder.find()

    print(event.message)

    for _, message in ipairs(commands) do

      -- begin: !e 100
      print(string.format("\t%s: %s", player.name, message))

      local splitMsg     = string_utils.mysplit(message, "%s+")
      local cmd          = string.gsub(splitMsg[1], "%s+", "")
      local string_args  = string.gsub(message, cmd, "")
      local args         = string_utils.mysplit(string_args, "%s+")
      local command_char = cmd:sub(1, 1)

      if command_char == "!" then
        local c = cmd:sub(2)
        print(string.format("\tAttempting to Execute Cmd: %s | Args: %s", c, string_args))
        exec(player, c, args)

        -- We should look this up less
        local mark = markers.find_marker(player, c)

        -- Pass in player
        if mark ~= nil then
          exec(player, "teleport", {c})
        end
      end

    end
end

return { handle = handle }
