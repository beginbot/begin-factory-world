-- ~/.factorio/script-output/notifications.txt
local function notify(message)
  game.write_file("notifications.txt", serpent.block(message))
end

local function flying_text(player, msg)
  local mod = math.random(-1, 1)
   player.create_local_flying_text(
    {
      text         = msg,
      position     = {
        player.position.x + ((math.random() * 4) * mod),
        player.position.y + ((math.random() * 4) * mod)
      },
      time_to_live = 500,
      -- color        = { r = 1, g = 0.5, b = 1, a = 1 }
      color        = {
        r = math.random(),
        g = math.random(),
        b = math.random(),
        a = math.random()
      }
   })
end


local function banner(player, args)
  local msg = args[1]
  if msg ~= nil then
    flying_text(player, msg)
  end
end


return {
  banner      = banner,
  flying_text = flying_text,
  notify      = notify,
}
