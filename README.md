# Begin Factory World

Beginbot's first Factorio Mod for allowing Twitch Chat to control the game.

## Full List of Commands

## Directions

You can move in a direction indefinitely or for a certain distance.

```
# Move indefinitely
!north
!n

# Move 10
!north 10
```

## Full List of Movement Commands

```
!dir X Y
!~
!stop

!e
!east
!n
!ne
!north
!northeast
!northwest
!nw
!s
!se
!south
!southeast
!southwest
!stopmine
!sw
!w
!west
```


## Build Commands


```
!craft
!build
!launch
!mine
!reset
!take
!put
```

```
!coal
!iron
```

```
!craft stone-furnace
!build stone-furnace
!destroy
```

## Wacky Commands

```
!oilmode
!firemode
!friend
!killall
!killmode
!banner
!biters
```

## Doesn't Work

```
!aboutface
!facesouth
```
