local direction     = require("direction")

local function rect(player, area)
  local right_bottom = area[1]
  local left_top = area[#area]
  local box = {
    right_bottom = right_bottom,
    left_top = left_top,
  }

  rendering.draw_rectangle{
    surface      = player.surface,
    right_bottom = right_bottom,
    left_top     = left_top,

    -- How should I make this random
    -- Or should I color this based on something useful
    color        = {1, 1, 0},
    width        = 2,
    time_to_live = 300,
  }
  return box
end

local function good_north(player, size)
  size = 2

  -- we choose an initial point
  -- -- Then we move base on direction
  local area = {
    {
      player.character.position.x-1,
      player.character.position.y
    },
    {
      player.character.position.x+1,
      player.character.position.y-2,
    }
  }
  return area
end


local function not_bad_west(player, size)
  local pos_offset = direction.dir_modifiers[player.character.direction]
  local x_offset = (pos_offset[1] * 1) + 1
  local y_offset = (pos_offset[2] * 1) + 1

  local area = {
    {
      player.character.position.x-2,
      player.character.position.y-2,
    },
    {
      player.character.position.x+(x_offset),
      player.character.position.y,
    }
  }
  return area
end

local function south_box_time(player, size)
  size = 2

  local area = {
    {
      player.character.position.x-size,
      player.character.position.y-size,
    },
    {
      player.character.position.x+size,
      player.character.position.y+size,
    }
  }
  return area
end

local function east_box_time(player, size)
  local posOffset = direction.dir_modifiers[player.character.direction]
  local xOffset = (posOffset[1] * 1) + 1
  local yOffset = (posOffset[2] * 1)

  local area = {
    {
      player.character.position.x-1,
      player.character.position.y-1,
    },
    {
      player.character.position.x+(xOffset),
      player.character.position.y+(yOffset + 1),
    }
  }
  return area
end


local function east_box(player, size)
  if size == nil then
    size = 2
  end
  local area = east_box_time(player, size)
  return rect(player, area)
end


local function west_box(player, size)
  if size == nil then
    size = 2
  end
  local area = not_bad_west(player, size)
  return rect(player, area)
end


local function north_box(player, size)
  if size == nil then
    size = 2
  end
  local area = good_north(player, size)
  return rect(player, area)
end


local function south_box(player, size)
  if size == nil then
    size = 2
  end
  local area = south_box_time(player, size)
  return rect(player, area)
end

local function box_on_player2(player, size)
  if size == nil then
    size = 1
  end
  -- Do we need this
  size = tonumber(size)

  local area = {
    {
      player.character.position.x,
      player.character.position.y,
    },
    {
      player.character.position.x-size,
      player.character.position.y-size,
    }
  }
  return area
end


-- Returns a Area around a player
-- Based on their direction
local function box_on_player(player, size)
  if size == nil then
    size = 1
  end
  -- Do we need this
  size = tonumber(size)

  -- local pos_offset = direction.dir_modifiers[player.character.direction]
  -- local pos_offset = direction.dir_modifiers[defines.direction.south]

  -- East is kinda nice
  local pos_offset = direction.dir_modifiers[defines.direction.east]
  local x_offset = (pos_offset[1] * 1) + size
  local y_offset = (pos_offset[2] * 1) + size

  local area = {
    {
      player.character.position.x-size,
      player.character.position.y-size,
    },
    {
      player.character.position.x+(x_offset),
      player.character.position.y+(y_offset)
    }
  }
  return area
end

local function box_in_front_of_player(player, size)
  -- if size == nil then
  --   size = 2
  -- end
  -- This allowed?
  size = size or 2

  local pos_offset = direction.dir_modifiers[player.character.direction]
  local x_offset   = pos_offset[1]
  local y_offset   = pos_offset[2]

  local area = {
    {
      player.character.position.x-1,
      player.character.position.y-1,
    },
    {
      player.character.position.x,
      player.character.position.y,
    },
  }

  for i=1,size do
    table.insert(area, {
      player.character.position.x+(x_offset - i),
      player.character.position.y+(y_offset - i),
    })

    table.insert(area, {
      player.character.position.x+(x_offset + i),
      player.character.position.y+(y_offset + i),
    })
  end

  return area
end

local function draw_box2(player, size)
  if size == nil then
    size = 2
  end
  local area = box_on_player(player, size)
  return rect(player, area)
end


local function draw_box(player, size)
  if size == nil then
    size = 2
  end
  local area = box_in_front_of_player(player, size)
  return rect(player, area)
end

return {
  box_in_front_of_player = box_in_front_of_player,
  box_on_player          = box_on_player,
  box_on_player2         = box_on_player2,
  rect                   = rect,
  draw_box               = draw_box,
  draw_box2              = draw_box2,
  east_box               = east_box,
  west_box               = west_box,
  north_box              = north_box,
  south_box              = south_box,
}
