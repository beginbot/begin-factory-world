local notifications = require("notifications")

local function toggle_map(player, args)
  local enable = global["map"]

  local size = args[1] or 1
  if size ~= nil then
    size = 0.05
  end
  if enable then
    global["map"] = nil
    player.close_map(player.position, size)
  else
    print("Enbaling Map")
    global["map"] = true
    player.open_map(player.position, size)
  end
end

local function find_markers(player, _)
  local surface = game.get_surface(1)
  return player.force.find_chart_tags(surface)

  -- local tags = player.force.find_chart_tags(surface)
  -- for key,tag in ipairs(tags) do
  --   local msg = string.format("%d - %s", key, tag.text)
  --   game.print(msg)
  --   print(msg)
  -- end
  -- return tags
end

local function find_marker(player, name)
  local markers = find_markers(player)

  for _, marker in ipairs(markers) do
    if marker.text == name then
      return marker
    end
  end

  return nil
end

local function create_marker(player, args)
	local name = args[1] or "Marker"
	local tag = { position = player.position, text = name }
  local old_marker = find_marker(player, name)

  -- Don't let people override tags
  if old_marker == nil then
	  local success = player.force.add_chart_tag("nauvis", tag)
    if success then
      local msg = string.format("Created Marker! %s", name)
      notifications.notify(msg)
    else
      local msg = string.format("Failed to Create Marker: %s", name)
      notifications.notify(msg)
    end
  else
    local msg = string.format("Failed to Create Marker. Old Marker found %s", name)
    notifications.notify(msg)
  end
end


return {
  create_marker = create_marker,
  find_marker   = find_marker,
  find_markers  = find_markers,
  toggle_map    = toggle_map,
}
