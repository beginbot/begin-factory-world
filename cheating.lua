local stolen        = require("stolen")
local build         = require("build")
local notifications = require("notifications")
local markers       = require("markers")
local direction     = require("direction")

local function enable_long_reach(player, _)
  local reach = 100000
  player.force.character_build_distance_bonus = reach
  player.force.character_reach_distance_bonus = reach
end

local function tel(player, args)
  local distance = args[1] or 1
  local pos      = player.position
  local dir_mod  = direction.dir_modifiers[player.character.direction]
  local x_mod    = dir_mod[1]
  local y_mod    = dir_mod[2]
  local new_pos  = {
    pos.x + (distance * x_mod),
    pos.y + (distance * y_mod),
  }
  player.teleport({(new_pos[1]+(distance * x_mod)), (new_pos[2]+(distance * y_mod))})
end

local function teleport(player, args)
  local name = args[1] or "base"

  print("Attempting to Teleport to " .. name)
  local marker = markers.find_marker(player, name)

  if marker ~= nil then
    player.teleport(marker.position)
  else
    local msg = string.format("Failed Teleport. No Marker %s found", name)
    notifications.notify(msg)
  end
end

local function reset(_, _)
    game.print("Resetting")
    global["oilmode"] = nil
    global["firemode"] = nil
    global["nuclear"] = nil
end

local function create(player, args)
    local potentialEntity = args[2]
    player.surface.create_entity({
      name     = potentialEntity,
      position = player.position,
  })
end

local function firemode(_, _)
    game.print("FIRE MODE TIME")
    global["firemode"] = true
end

local function nuclear_explosion(player, _)
   for y=-2,2 do
     for x=-2,2 do
       game.surfaces.nauvis.create_entity({ name="nuke-explosion", amount=5000, position={player.position.x+x,player.position.y+y} })
     end
 end
end

local function create_biters(player, _)
  player.surface.create_entity{
    name = "behemoth-biter",
    position = {
      player.position.x + 15,
      player.position.y
    }
  }
end

local function killmode(_, _)
    game.print("KILL MODE TIME")
    global["killmode"] = true
end

local function oilmode(_, _)
    game.print("OIL MODE TIME")
    global["oilmode"] = true
end

local function nuclear(_, _)
    game.print("Nuclear TIME")
    global["nuclear"] = true
end

local function launch(player, _)
    stolen.launch(player, player.position)
end

local function kill(_, _)
    game.forces['enemy'].kill_all_units()
end

local function friend(player, _)
    game.surfaces[1].create_entity{
      name     = "big-biter",
      position = {player.position.x + 10, player.position.y + 10},
      force    = game.forces.player,
    }
end

local function squad_up(player, _)
  math.randomseed(game.tick)
  for y = -10, 10 do
    for x = -10, 10 do
      game.surfaces.nauvis.create_entity( { name = "character", amount = 1, position = { player.position.x + x * 2 + math.random(1, 10), player.position.y + y + y * 2 + math.random(1, 10) } } )
    end
  end
end

local function destroy_squad(player, _)
  print("LETS DESTROY THE SQUAD")
  local chars = player.surface.find_entities_filtered{
    area={{
      player.position.x-100, player.position.y-100},
      {player.position.x+100, player.position.y+100}},
      name="character"
    }
   for _,entity in pairs(chars) do
     entity.destroy()
   end

  -- for _, ent in player.surface.find_entities_filtered{{{
  --   (player.position.x)-100, (player.position.y)-100}, {(player.position.x)+100, (player.position.y)+100} , name= "character"}} do
  --   ent.destroy()
  -- end

  -- for _, ent in player.surface.find_entities_filtered{
  --   area={
  --     { game.player.position.x-1000, game.player.position.y-1000 },
  --     { game.player.position.x+1000, game.player.position.y+1000 }
  --   }, type="character" } do
  --     ent.destroy()
  --   end
end

local function killall(entity_type)
  for _, entity in ipairs(
    game.player.surface.find_entities_filtered{
      area={
        { game.player.position.x-1000, game.player.position.y-1000 },
        { game.player.position.x+1000, game.player.position.y+1000 }
      },
        type=entity_type,
      }
    ) do
    entity.destroy()
  end
end

local function hit_the_road2(player, _)
  local args = { "transport-belt", 50 }
  build.build_the_road(player, args)
end

local function hit_the_road(player, _)
  -- local args = { "transport-belt", 50 }
  local args = { "transport-belt", 0, 0, 50}
 -- !build tb 0 0 20
  -- for i=1,50 do
  build.build_entity(player, args)
end

return {
  create            = create,
  create_biters     = create_biters,
  firemode          = firemode,
  friend            = friend,
  kill              = kill,
  killall           = killall,
  killmode          = killmode,
  launch            = launch,
  oilmode           = oilmode,
  nuclear           = nuclear,
  reset             = reset,
  teleport          = teleport,
  tel               = tel,
  hit_the_road      = hit_the_road,
  hit_the_road2     = hit_the_road2,
  enable_long_reach = enable_long_reach,
  nuclear_explosion   = nuclear_explosion,
  destroy_squad = destroy_squad,
  squad_up            = squad_up,
}
