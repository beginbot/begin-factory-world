local Cache = {
}

Cache.__index=function(k,_)
    print("Couldn't find index for: " .. k)
    return {}
end

local function add(c, action, args)
  c[action] = args
end

-- build
-- build stone-furnace
local function read(c, action)
  return c[action]
end

return {
  add  = add,
  read = read,
  Cache = Cache,
}
