local direction     = require("direction")
local box           = require("box")
local stolen        = require("stolen")
local notifications = require("notifications")

local EntityShortcuts = {
    ["sf"]  = "stone-furnace",
    ["ec"]  = "electronic-circuit",
    ["gec"] = "electronic-circuit",
    ["rec"] = "red-electronic-circuit",
    ["wc"]  = "wooden-chest",
    ["ic"]  = "iron-chest",
    ["i"]   = "inserter",
    ["lhi"] = "long-handed-inserter",
    ["ip"]  = "iron-plate",
    ["fi"]  = "fast-inserter",
    ["io"]  = "iron-ore",
    ["igw"]  = "iron-gear-wheel",
    ["co"]  = "copper-ore",
    ["cc"]  = "copper-cable",
    ["cp"]  = "copper-plate",
    ["bi"]  = "burner-inserter",
    ["tb"]  = "transport-belt",
    ["bmd"] = "burner-mining-drill",
    ["emd"] = "electric-mining-drill",
    ["op"]  = "offshore-pump",
    ["am1"] = "assembling-machine-1",
    ["am2"] = "assembling-machine-2",
    ["am3"] = "assembling-machine-3",
    ["sep"] = "small-electric-pole",
    ["red-science"] = "automation-science-pack",
    ["red-sci"] = "automation-science-pack",
    ["redsci"] = "automation-science-pack",

    -- dead-dry-hairy-tree dead-grey-trunk dead-tree dry-hairy-tree dry-tree green-coral tree-01 tree-02 tree-02-red tree-03 tree-04 tree-05 tree-06 tree-06-brown tree-07 tree-08 tree-08-brown tree-08-red tree-09 tree-09-brown tree-09-red
}

local Slots = {
  ["stone-furnace"] = {
    ["put"] = {
      ["default"] = defines.inventory.furnace_source,
      ["coal"]    = defines.inventory.fuel,
    },
    ["take"] = {
      ["default"] = defines.inventory.furnace_result,
    }
  },

  ["iron-chest"] = {
    ["default"] = defines.inventory.chest,
  },

  ["wooden-chest"] = {
    ["default"] = defines.inventory.chest,
  },
  ["lab"] = {
    ["default"] = defines.inventory.lab_input,
  },
}

local LayDistances = {
  ["small-electric-pole"] = 5,
  ["pipe"]                = 0.8,
  ["stone-furnace"]       = 2,
  ["inserter"]            = 2,
}

local function _find_entity(raw_entity)
    raw_entity   = string.gsub(raw_entity, "%s+", "")
    local entity = EntityShortcuts[raw_entity]

    if entity ~= nil then
      return entity
    else
      return raw_entity
    end
end

local function find_slot(entity, source, action)
  local source_slots = Slots[source]
  if source_slots ~= nil then
    local action_slots = source_slots[action]

    if action_slots ~= nil then
      local slot = action_slots[entity]
      if slot ~= nil then
        return slot
      else
        return action_slots["default"]
      end
    else
      return source_slots["default"]
    end
  end
end

local function mine(player, _)
    global["minemode"] = true
    player.update_selected_entity(player.position)
    player.mining_state = {mining = true, position = player.position}
end

local function stopmine(player, _)
    player.walking_state = {walking = false, direction = defines.direction.north}
    global["minemode"] = nil
end

local function player_position_builder(player, args)
  local new_pos

  -- If we pass in 3 args, we assume an entity
  -- and Cartesian coordinates
  if #args == 3 then
    local x_offset = args[2]
    local y_offset = args[3] * -1

    new_pos = {
      player.position.x + x_offset,
      player.position.y + y_offset,
    }

    local msg = string.format(
      "Current: %d %d | New: %d %d",
      player.position.x,
      player.position.y,
      new_pos[1],
      new_pos[2])
    print(msg)

  else
    local dir_mod = direction.dir_modifiers[player.character.direction]
    local x_mod = dir_mod[1]
    local y_mod = dir_mod[2]
    local distance
    distance = args[2]
    if distance == nil then
      distance = 1
    end
    distance = distance

    new_pos = {
      player.position.x + (distance * x_mod),
      player.position.y + (distance * y_mod),
    }
  end

  return new_pos
end

local function position_builder(player, init_pos, args)
  local new_pos

  local init_x = init_pos.x
  local init_y = init_pos.y

  -- If we pass in 3 args, we assume an entity
  -- and Cartesian coordinates
  if #args == 3 then
    local x_offset = args[2]
    local y_offset = args[3] * -1

    new_pos = {
      init_x + x_offset,
      init_y + y_offset,
    }

    local msg = string.format(
      "Current: %d %d | New: %d %d",
      init_x,
      init_y,
      new_pos[1],
      new_pos[2])
    print(msg)

  else
    local dir_mod = direction.dir_modifiers[player.character.direction]
    local x_mod = dir_mod[1]
    local y_mod = dir_mod[2]
    local distance
    distance = args[2]
    if distance == nil then
      distance = 1
    end
    distance = distance

    new_pos = {
       init_x + (distance * x_mod),
       init_y + (distance * y_mod),
    }
  end

  return new_pos
end


-- !build
--    args: entity x y amount
local function build_entity(player, args)
    local entity  = _find_entity(args[1])
    local new_pos = player_position_builder(player, args)
    local amount  = args[4] or 1

    -- Does this amount work??
    for i=1,amount do
      local success = stolen.build(
        player,
        new_pos,
        entity,
        player.character.direction
      )
      local msg = string.format(
        "Building: %s %d %d %s",
        entity,
        new_pos[1],
        new_pos[2],
        player.character.direction)
      game.print(msg)
      if not success then
        game.print("Unsuccessful build of " .. entity .. " " .. tostring(i))
      end
    end
end

-- This only works for North
-- !lay pipe 5
-- !lay sep  5
local function lay_items(player, args)
  local source         = _find_entity(args[1])
  local amount         = args[2] or 5
  local potential_dist = args[3]
  local distance       = LayDistances[source]
  if distance == nil then
    distance = potential_dist or 1
  end

  local pos      = player.position
  local dir_mod  = direction.dir_modifiers[player.character.direction]
  local x_mod    = dir_mod[1]
  local y_mod    = dir_mod[2]

  -- direction.move_backwards(player, 1)
  -- Set back
  -- Move backwards
  local new_pos
  for i=0,amount do
    -- We can build pipes under players???
    -- but we can't build poles???
    new_pos = {
      pos.x + ((i * distance) * x_mod),
      pos.y + ((i * distance) * y_mod),
    }

    local success = stolen.build(
      player,
      new_pos,
      source,
      player.character.direction
    )
    if success then
      -- local player_pos = {(new_pos[1]+(distance * x_mod)), (new_pos[2]+(distance * y_mod))}
      -- direction.move_in_dir(player, player.character.direction, player_pos)
      -- player.teleport(
      -- )

      -- We need to increase slightly!
      -- player.teleport(new_pos)
      -- Or we choose one based on direction
      player.teleport({(new_pos[1]+(distance * x_mod)), (new_pos[2]+(distance * y_mod))})
    end
  end

  -- I want to be able to add 1 in a direction
  -- player.teleport(new_pos)

  -- This doesn't make it easy to place
  -- player.teleport({(new_pos[1]+(1 * x_mod)), (new_pos[2]+(1 * y_mod))})
  -- TODO: Maybe make this a mode
  -- direction.move_in_dir(player, player.character.direction, new_pos)
  -- player.teleport(new_pos)
end

local function build_the_road(player, args)
    local entity = _find_entity(args[1])
    local amount
    amount = args[2]
    if amount == nil then
      amount = 1
    end

    local pos = player.position
    local dir_mod = direction.dir_modifiers[player.character.direction]
    local x_mod = dir_mod[1]
    local y_mod = dir_mod[2]

    for i=1,amount do
      local newPos = {
        pos.x + ((i - 1) * x_mod),
        pos.y + ((i - 1) * y_mod),
      }
      -- local newPos = pos

      local success = stolen.build(player, newPos, entity, player.character.direction)
      if not success then
        return "Unsuccessful build of " .. entity .. " | Number: " .. tostring(i)
      end
    end
end

-- This probably wants all
-- https://lua-api.factorio.com/0.14.7/LuaControl.html#LuaControl.get_craftable_count
local function craft_entity(player, args)
    local entity          = _find_entity(args[1])
    local craftable_count = player.get_craftable_count(entity)
    local amount          = args[2] or 1

    if craftable_count > 0 then
      local a = math.min(craftable_count, amount)
      stolen.craft(player, a, entity)
    else
      -- Not sure we want this
      -- local msg = string.format("Not Enough Ingredients to Craft %s", entity)
      -- notifications.notify(msg)
    end
end

-- We could find the specific entity, if we know the name
-- https://lua-api.factorio.com/0.12.35/LuaSurface.html#find_entity
local function destroy(player, args)
  local source = _find_entity(args[1])
  local size = args[2] or 1

  print(string.format("Destroying %s %d", source, size))

  local area = box.box_on_player(player, size)
  box.rect(player, area)

  -- We could add a no filter mode
  -- local entities = game.surfaces["nauvis"].find_entities(box)
  local entities = game.get_surface(1).find_entities_filtered{
    area = area,
    name = {source},
  }

  for _, entity in ipairs(entities) do
    -- if entity.mineable then
      local success = player.mine_entity(entity)
      if not success then
          print(string.format("Failed to Destroy %s", entity.name))
      end
    -- end
  end
end

-- We might deprecate this
local function rotate(player, _)
  local pos = direction.right_in_front_of_player(player)
  local farPos = direction.right_in_front_of_player(player, 2)

  local entities = game.surfaces["nauvis"].find_entities({pos, farPos})
  for _, entity in ipairs(entities) do

    if entity.name ~= "character" then
      print("Rotating ... " .. entity.name)
      if entity.rotatable then
        local success = entity.rotate()
        if not success then
          print(string.format("Failed to Rotate %s" ,entity.name))
        end
      else
        print(string.format("%s Not Rotatable" ,entity.name))
      end
    end
  end
end

local function _rot(player, reverse, size, source)
  local area = box.box_on_player(player, size)
  box.rect(player, area)

  local entities
  if source ~= nil then
    entities = game.get_surface(1).find_entities_filtered{
      area = area,
      name = {source},
    }
  else
    entities = game.surfaces["nauvis"].find_entities(area)
  end

  for _, entity in ipairs(entities) do
    print(entity.name)
    if entity.name ~= "character" then
      print("Rotating" .. entity.name)

      if entity.rotatable then
        local success = entity.rotate({
          ["reverse"] = reverse,
        })
        if not success then
          print(string.format("Failed to Rotate %s" ,entity.name))
        end
      else
        notifications.notify(string.format("%s Not Rotatable" ,entity.name))
      end
    end
  end
end


-- !rot 2
-- !rot bi
-- !rot bi 2
-- !rot 2 bi
local function rrot(player, args)
  local entity_name = _find_entity(args[1])
  local size        = args[2] or 1
  _rot(player, true, size, entity_name)
end

-- reverse
local function rot(player, args)
  local entity_name = _find_entity(args[1])
  local size        = args[2] or 1
  _rot(player, false, size, entity_name)
end

-- Things to Explore
-- entity.get_output_inventory()
-- inventory.get_item_count()
-- inventory.remove({name=itemName, count=amount})
--
-- !take iron-plate sf
local function take(player, args)
  local entity_name = _find_entity(args[1])
  local source      = _find_entity(args[2])
  local amount      = args[4] or 100
  local slot        = find_slot(entity_name, source, "take") or 1

  local size = args[3] or 4

  local area = box.box_on_player(player, size)
  box.rect(player, area)

  local entities = game.get_surface(1).find_entities_filtered{
    area = area,
    name = {source},
  }

  for _, entity in ipairs(entities) do
    print(string.format("%s taking %d %s in %s", player.name, amount, entity_name, source))

    stolen.take(
      player,
      {entity.position.x, entity.position.y},
      entity_name,
      amount,
      slot
    )
  end
end

local function flip(player, _)
  _rot(player, false)
  _rot(player, false)
end

local function put_item(player, args)
  local entity_name = _find_entity(args[1])
  local source      = _find_entity(args[2])
  local slot        = find_slot(entity_name, source, "put") or 1
  local amount      = args[4] or 100
  local size        = args[3] or 4

  local area = box.box_on_player(player, size)
  box.rect(player, area)
  local entities = game.get_surface(1).find_entities_filtered{
    area = area,
    name = {source},
  }

  amount = amount / #entities

  for _, entity in ipairs(entities) do
    print(string.format("%s taking %d %s in %s", player.name, amount, entity_name, source))

    stolen.put(
      player,
      { entity.position.x, entity.position.y },
      entity_name,
      amount,
      slot
    )
  end
end

local function research(player, args)
  tech = args[1]
  local success = stolen.tech(player, tech)
  if success then
    notifications.notify(string.format("Researching %s", tech))
  end
end


local function find_ingredients(player, args)
  local entity_name = _find_entity(args[1])
  local rec = player.force.recipes[entity_name]

  local names = {}

  -- type :: string: "item" or "fluid".
  -- minimum_temperature :: double : The minimum fluid temperature required. Has no effect if type is '"item"'.
  -- maximum_temperature :: double : The maximum fluid temperature allowed. Has no effect if type is '"item"'.
  -- catalyst_amount :: uint or double: How much of this ingredient is a catalyst.

  for i, ingredient in pairs(rec.ingredients) do
    names[i] = string.format("%s: %d", ingredient.name, ingredient.amount)
  end
  local msg = table.concat(names, ", ")
  notifications.notify(msg)
end

-- !recipe am1 redsci SIZE
-- !recipe ENTITY RECIPE SIZE
local function set_recipe(player, args)
  local entity_name = _find_entity(args[1])
  local recipe      = _find_entity(args[2])
  local size        = args[3] or 2

  local area = box.box_on_player(player, size)
  box.rect(player, area)

  local entities = game.get_surface(1).find_entities_filtered{
    area = area,
    name = {entity_name},
  }
  for _, entity in ipairs(entities) do
    print(string.format("%s %s", entity.name, recipe))
    entity.set_recipe(recipe)
  end
end

local function move(player, args)
  local entity_name = _find_entity(args[1])
  -- local size        = args[2] or 2
  local size        = args[4] or 2
  local amount = 1

  -- We need to delete and recreate
  local area = box.box_on_player(player, size)
  box.rect(player, area)

  local entities = game.get_surface(1).find_entities_filtered{
    area = area,
    name = {entity_name},
  }
  for _, entity in ipairs(entities) do
    local new_pos = position_builder(player, entity.position, args)
    -- local success = player.mine_entity(entity)
    player.mine_entity(entity)
    stolen.build(
      player,
      new_pos,
      entity_name,
      player.character.direction
    )
  end
end

return {
  build_entity   = build_entity,
  build_the_road = build_the_road,
  craft_entity   = craft_entity,
  destroy        = destroy,
  flip           = flip,
  lay_items      = lay_items,
  mine           = mine,
  put_item       = put_item,
  rot            = rot,
  rotate         = rotate,
  rrot           = rrot,
  stopmine       = stopmine,
  take           = take,
  research       = research,
  set_recipe     = set_recipe,
  find_ingredients = find_ingredients,
  move = move,
}
