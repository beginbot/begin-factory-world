local function find()
    local player = game.get_player(1)
    if player == nil then
      print("PLAYER IS NIL FOR SOME REASON")
      return game.get_player(2)
    end
    return player
end

return {
  find = find,
}
