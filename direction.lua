require ("util")

local stolen = require("stolen")
local missing = require("missing")

local dir_modifiers = {
  [defines.direction.north]     = {0, -1},
  [defines.direction.south]     = {0, 1},
  [defines.direction.east]      = {1, 0},
  [defines.direction.west]      = {-1, 0},
  [defines.direction.northeast] = {1, -1},
  [defines.direction.northwest] = {-1, -1},
  [defines.direction.southeast] = {1, 1},
  [defines.direction.southwest] = {-1, 1},
}

local opposites = {
  [defines.direction.north]     = defines.direction.south,
  [defines.direction.south]     = defines.direction.north,
  [defines.direction.east]      = defines.direction.west,
  [defines.direction.west]      = defines.direction.east,
  [defines.direction.northeast] = defines.direction.southwest,
  [defines.direction.northwest] = defines.direction.southeast,
  [defines.direction.southeast] = defines.direction.northwest,
  [defines.direction.southwest] = defines.direction.northeast,
}

local function stop(player, _)
    global["minemode"] = nil
    player.walking_state = {walking = false, direction = defines.direction.north}
end

local function stop_everything(player, _)
    player.walking_state = {walking = false, direction = defines.direction.north}
    global["minemode"] = false
end


local function move_in_dir(player, dir, args)
  print("\tMove In Dir")
  print(missing.inspect(args))

  player.mining_state = {mining = false, position = player.position}
  global["minemode"] = false

  -- Are they facing the direction they want to go?
  if player.character.direction ~= dir then
    player.walking_state = {walking = true, direction = dir}
  end

  player.walking_state = {walking = true, direction = dir}

  --
  local raw_mod_value = args[1]
  if raw_mod_value == nil then return end

  local mods  = dir_modifiers[dir]
  local x_mod = mods[1]
  local y_mod = mods[2]

  print("Setting Walking State")

  global["player.walking"] = nil
  local mod_val = tonumber(raw_mod_value)
  local x = player.position.x + (mod_val * x_mod)
  local y = player.position.y + (mod_val * y_mod)

  global["player.walking"] = { ["x"] = x, ["y"] = y }
end

local function dir(player, args)
    global["player.walking"] = nil
    local rawX = args[1]
    local rawY = args[2]
    local x = tonumber(rawX)
    local y = tonumber(rawY)

    player.walking_state = stolen.walk(x,y)
    local newPos = {
      ["x"] = (player.position.x + x),
      ["y"] = (player.position.y + y),
    }
    global["player.walking"] = newPos
end


local function move_east(player, args)
  move_in_dir(player, defines.direction.east, args)
end

local function move_west(player, args)
  move_in_dir(player, defines.direction.west, args)
end

local function move_south(player, args)
  move_in_dir(player, defines.direction.south, args)
end

local function move_north(player, args)
  move_in_dir(player, defines.direction.north, args)
end

local function move_southeast(player, args)
  move_in_dir(player, defines.direction.southeast, args)
end

local function move_southwest(player, args)
  move_in_dir(player, defines.direction.southwest, args)
end

local function move_northwest(player, args)
  move_in_dir(player, defines.direction.northwest, args)
end

local function move_northeast(player, args)
  move_in_dir(player, defines.direction.northeast, args)
end

local function right_in_front_of_player(player, modifier)
    if modifier  == nil then
      modifier = 1
    end

    local posOffset = dir_modifiers[player.character.direction]
    return {
      player.character.position.x+(posOffset[1] * modifier),
      player.character.position.y+(posOffset[2] * modifier)
    }
end

local function move_backwards(player, amount)
  local op_dir = opposites[player.character.direction]
  move_in_dir(player, op_dir, {amount})
end

return {
  dir                      = dir,
  dir_modifiers            = dir_modifiers,
  move_in_dir              = move_in_dir,
  move_east                = move_east,
  move_north               = move_north,
  move_northeast           = move_northeast,
  move_northwest           = move_northwest,
  move_south               = move_south,
  move_southeast           = move_southeast,
  move_southwest           = move_southwest,
  move_west                = move_west,
  right_in_front_of_player = right_in_front_of_player,
  stop                     = stop,
  stop_everything          = stop_everything,
  move_backwards           = move_backwards,
}
