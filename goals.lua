-- We need to extract out the goals
-- and use the goals in the story updater
-- and the displayer

local player_finder = require("player_finder")

local supply_goals =
{
  ["coal"]     = 20,
  ["iron-ore"] = 20,
  ["stone"]    = 20,
}


-- local update_materials_gui = function(gui)
local function update_materials_gui(gui)
  local player = player_finder.find()

  local src    = player.get_inventory(defines.inventory.character_main)

  local coal_count  = 0
  local iron_count  = 0
  local stone_count = 0

  if src ~= nil then
    coal_count  = src.get_item_count("coal")
    iron_count  = src.get_item_count("iron-ore")
    stone_count = src.get_item_count("stone")
  end

  local contents = {
    ["coal"]     = coal_count,
    ["iron-ore"] = iron_count,
    ["stone"]    = stone_count,
  }

  local table = gui.holding_table
  if not table then
    table = gui.add{type = "table", column_count = 3, style = "bordered_table", name = "holding_table"}
  end

  local items = game.item_prototypes

  for item, count in pairs(supply_goals) do
    if items[item] then
      local count_label = table[item]
      if not count_label then
        local sprite = table.add{type = "sprite", sprite = "item/"..item}
        sprite.style.width = 32
        sprite.style.height = 32
        local item_label = table.add{type = "label", caption = items[item].localised_name, style = "bold_label"}
        item_label.style.horizontally_stretchable = false
        count_label = table.add{type = "label", name = item}
      end
      local current_count = contents[item] or 0
      count_label.caption = current_count.."/"..count
      if current_count >= count then
        count_label.style = "bold_green_label"
      else
        count_label.style = "label"
      end
    end
  end
end

local function has_completed_goals()
  local player = player_finder.find()
  local src = player.get_inventory(defines.inventory.character_main)

  if src ~= nil then
    for entity, desired_amount in pairs(supply_goals) do
      local count = src.get_item_count(entity)

      -- Does this early return in LUA
      -- Will this return from the whole function
      if count < desired_amount then return false end
    end
  end

  return true
end

local function set_goal(goal)
    local player = player_finder.find()

    if player ~= nil then
      player.set_goal_description(goal)
    end
end

return {
  set_goal             = set_goal,
  update_materials_gui = update_materials_gui,
  has_completed_goals  = has_completed_goals,
}
